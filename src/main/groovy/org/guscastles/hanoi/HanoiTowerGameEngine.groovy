package org.guscastles.hanoi

import groovy.transform.TailRecursive

class HanoiTowerGameEngine implements Visitor {

  HanoiTower hanoiTowerHost

  @Override
  def visits(HanoiTower hanoiTower) {
    hanoiTowerHost = hanoiTower
    play()
  }

  @TailRecursive
  def play(Disc currentDisc, Integer movesCounter) {
    if (movesCounter < hanoiTowerHost.numberOfMoves()) {
      Boolean okToMoveToNextRod = canDiscBeMovedToNextRod currentDisc
      if (okToMoveToNextRod) moveToNextRod currentDisc
      play(getNextDisc(currentDisc), (okToMoveToNextRod ? movesCounter + 1 : movesCounter))
    }
  }

  void play() {
    hanoiTowerHost.printRods()
    if (isThereAnyDisc()) {
      Disc currentDisc = hanoiTowerHost.discs[0]
      play(currentDisc, 0)
    }
  }

  Boolean canDiscBeMovedToNextRod(Disc currentDisc) {
    def nextRod = currentDisc.nextRod()
    (isNextRodEmpty(nextRod) || isDiscOntoNextRodBigger(nextRod, currentDisc)) &&
      isCurrentDiscTopOfCurrentRod(currentDisc)
  }

  Boolean isThereAnyDisc() {
    hanoiTowerHost.discs.size() > 0
  }

  Boolean isNextRodEmpty(def rod) {
    hanoiTowerHost.rods."$rod".size() == 0
  }

  def moveToNextRod(def currentDisc) {
    def currentRod = currentDisc.currentRod
    def nextRod = currentDisc.nextRod()
    currentDisc.currentRod = nextRod
    hanoiTowerHost.rods."$nextRod".push(hanoiTowerHost.rods."$currentRod".pop())
    hanoiTowerHost.printRods()
  }

  Boolean isDiscOntoNextRodBigger(Rod rod, Disc disc) {
    hanoiTowerHost.rods."$rod".size() > 0 && hanoiTowerHost.rods."$rod"[-1].order > disc.order
  }

  Boolean isCurrentDiscTopOfCurrentRod(Disc disc) {
    def rod = disc.currentRod
    hanoiTowerHost.rods."$rod"[-1] == disc
  }

  Disc getNextDisc(Disc disc) {
    def index = disc.order - 1
    hanoiTowerHost.discs[index == hanoiTowerHost.discs.size() - 1 ? 0 : index + 1]
  }
}
