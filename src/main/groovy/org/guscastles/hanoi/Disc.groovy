package org.guscastles.hanoi

class Disc {
  Integer order
  List<Rod> rods
  Rod currentRod

  @Override
  String toString() {
    order
  }

  Rod nextRod() {
    def nextIndex = rods.indexOf(currentRod) + 1
    if (currentRod == null) currentRod = rods[0]
    else nextIndex <= 2 ? rods[nextIndex] : rods[0]
  }
}
