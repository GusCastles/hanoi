package org.guscastles.hanoi

enum Rod {
  FROM(0), TO(1), EXTRA(2)

  Integer order

  Rod(def order) {
    this.order =  order
  }
}
