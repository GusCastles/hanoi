package org.guscastles.hanoi

import static Rod.*

class HanoiTower {

  List<Disc> discs
  Map<Rod, List<Disc>> rods
  Visitor gameEngine

  HanoiTower() {}

  HanoiTower(Integer numberOfDiscs, Integer initialRod = 0) {
    discs = []
    if (numberOfDiscs > 0) (1..numberOfDiscs).each {discs.add new Disc(order: it)}
    createRods(initialRod)
  }

  void createRods(def initialRod = 0) {
    assert discs != null
    rods = [FROM: [], TO: [], EXTRA: []]
    def i = 0
    discs.each {disc ->
      def discNumber = discs.size() - i - 1
      if (isDiscAvailable(discNumber)) discs.putAt(discNumber, new Disc(order: discNumber + 1))
      discs[discNumber].rods = getRodsSequence(i)
      rods.FROM[i++] = discs[discNumber]
    }
  }

  Boolean isDiscAvailable(Integer discNumber) {
    discs[discNumber] == null || discs[discNumber].order == null
  }

  def getRodsSequence(def order) {
    order % 2 == 1 ? [FROM, EXTRA, TO] : [FROM, TO, EXTRA]
  }

  void printRods() {
    println rods
  }

  BigDecimal numberOfMoves() {
    2.power(discs.size()) - 1
  }

  def play() {
    gameEngine.visits(this)
  }
}