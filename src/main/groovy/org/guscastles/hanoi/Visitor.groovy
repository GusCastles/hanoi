package org.guscastles.hanoi

trait Visitor {
  abstract visits(HanoiTower host)
}
