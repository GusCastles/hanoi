package org.guscastles.hanoi

import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.ExpectedException

import static org.hamcrest.CoreMatchers.*
import static org.hamcrest.MatcherAssert.*
import static Rod.*

class HanoiTowerTest {

  def hanoiTower

  @Rule
  public ExpectedException expectedException = ExpectedException.none()

  @Before
  void setup() {
    hanoiTower = new HanoiTower()
  }

  @Test
  void hanoiTowerDiscs_ShouldBeAList() {
    hanoiTower.discs = []
    assertThat(hanoiTower.discs.class, equalTo(ArrayList))
  }

  @Test
  void numberOfMoves_ShouldFollowTheFormula2ToThePowerOfNMinus1() {
    assertThat(new HanoiTower(1).numberOfMoves(), equalTo(1.toBigDecimal()))
    assertThat(new HanoiTower(2).numberOfMoves(), equalTo(3.toBigDecimal()))
    assertThat(new HanoiTower(3).numberOfMoves(), equalTo(7.toBigDecimal()))
    assertThat(new HanoiTower(4).numberOfMoves(), equalTo(15.toBigDecimal()))
  }

  @Test
  void hanoiTowerWith3Discs_ShouldHave3DiscObjects() {
    hanoiTower.discs = [new Disc(), new Disc(), new Disc()]
    assertThat(hanoiTower.discs.size(), equalTo(3))
  }

  @Test
  void aDisc_ShouldHaveAnOrderNumber() {
    def disc = new Disc(order: 1)
    assertThat(disc.order, equalTo(1))
  }

  @Test
  void aDisc_ShouldHaveAMovesPattern() {
    def disc = new Disc(order: 1)
    disc.rods = disc.order % 2 == 0 ? [FROM, EXTRA, TO] : [FROM, TO, EXTRA]
    assertThat(disc.rods, equalTo([FROM, TO, EXTRA]))
  }

  @Test
  void aDiscNumber1_ShouldGiveTheNextMoveFollowingThePattern() {
    def disc = new Disc(order: 1)
    disc.rods = disc.order % 2 == 0 ? [FROM, EXTRA, TO] : [FROM, TO, EXTRA]
    assertThat(disc.rods, equalTo([FROM, TO, EXTRA]))
    assertThat(disc.nextRod(), equalTo(FROM))
    assertThat(disc.nextRod(), equalTo(TO))
    disc.currentRod = TO
    assertThat(disc.nextRod(), equalTo(EXTRA))
    disc.currentRod = EXTRA
    assertThat(disc.nextRod(), equalTo(FROM))
  }

  @Test
  void aDiscNumber1_ShouldBeAbleToRevertTheMoveToThePreviousOne() {
    def disc = new Disc(order: 1)
    disc.rods = disc.order % 2 == 0 ? [FROM, EXTRA, TO] : [FROM, TO, EXTRA]
    assertThat(disc.rods, equalTo([FROM, TO, EXTRA]))
    assertThat(disc.nextRod(), equalTo(FROM))
    assertThat(disc.nextRod(), equalTo(TO))
  }

  @Test
  void hanoiTower_ShouldHaveTheInitialRodsSetup() {
    hanoiTower = new HanoiTower(3)
    hanoiTower.createRods()
    assertThat(hanoiTower.rods.toString(), equalTo([FROM: [3, 2, 1], TO: [], EXTRA: []].toString()))
    assertThat(hanoiTower.discs[0].rods, equalTo([FROM, TO, EXTRA]))
  }

  @Test
  void hanoiTower_ShouldHaveTheInitialDiscsSetup() {
    hanoiTower = new HanoiTower(1)
    assertThat(hanoiTower.discs.size(), equalTo(1))
    assertThat(hanoiTower.discs.toString(), equalTo([new Disc(order: 1)].toString()))
    hanoiTower = new HanoiTower(2)
    assertThat(hanoiTower.discs.size(), equalTo(2))
    hanoiTower = new HanoiTower(1)
    assertThat(hanoiTower.discs.size(), equalTo(1))
  }

  @Test
  void rodsShouldBeAMap(){
    hanoiTower = new HanoiTower(1)
    assertThat(hanoiTower.rods.FROM.toString(), equalTo([new Disc(order: 1)].toString()))
  }
}