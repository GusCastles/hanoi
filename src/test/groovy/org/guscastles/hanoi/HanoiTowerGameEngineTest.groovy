package org.guscastles.hanoi

import static org.hamcrest.CoreMatchers.*
import static org.hamcrest.MatcherAssert.*
import org.junit.*

class HanoiTowerGameEngineTest {

  def hanoiTower
  def hanoiTowerGameEngine

  @Before
  void setup() {
    hanoiTower = new HanoiTower()
    hanoiTowerGameEngine = new HanoiTowerGameEngine()
  }

  @Test
  void visitor_ShouldBePresent() {
    hanoiTower.gameEngine = hanoiTowerGameEngine
    assertThat(hanoiTower.gameEngine, notNullValue())
  }

  @Test
  void gameEngine_ShouldPlayTheGame() {
    hanoiTower = new HanoiTower(3)
    hanoiTowerGameEngine.visits(hanoiTower)
    assertThat(hanoiTower.rods.toString(), equalTo([FROM: [], TO: [3, 2, 1], EXTRA: []].toString()))
  }

  private playGame(def numberOfDiscs) {
    hanoiTower = new HanoiTower(numberOfDiscs)
    assertThat(hanoiTower.discs.size(), equalTo(numberOfDiscs))
    hanoiTower.gameEngine = hanoiTowerGameEngine
    hanoiTower.play()
  }

  @Test
  void playGameWith1Disc_ShouldReturnRightResult() {
    hanoiTower = new HanoiTower(1)
    assertThat(hanoiTower.discs.size(), equalTo(1))
    assertThat(hanoiTower.rods.toString(), equalTo([FROM: [1], TO: [], EXTRA: []].toString()))
    hanoiTower.gameEngine = hanoiTowerGameEngine
    hanoiTower.play()
    assertThat(hanoiTower.rods.toString(), equalTo([FROM: [], TO: [1], EXTRA: []].toString()))
  }

  @Test
  void playingGameWithUpTo2Discs_ShouldReturnCorrectResult() {
    playGame(0)
    assertThat(hanoiTower.rods.toString(), equalTo([FROM: [], TO: [], EXTRA: []].toString()))
    playGame(2)
    assertThat(hanoiTower.rods.toString(), equalTo([FROM: [], TO: [2,1], EXTRA: []].toString()))
  }

  @Test
  void playingGameWith3Discs_ShouldReturnCorrectResult() {
    playGame(3)
    assertThat(hanoiTower.rods.toString(), equalTo([FROM: [], TO: [3,2,1], EXTRA: []].toString()))
  }

  @Test
  void playingGameWithMoreThan3Discs_ShouldReturnCorrectResult() {
    playGame(4)
    assertThat(hanoiTower.rods.toString(), equalTo([FROM: [], TO: [4,3,2,1], EXTRA: []].toString()))
    playGame(5)
    assertThat(hanoiTower.rods.toString(), equalTo([FROM: [], TO: [5,4,3,2,1], EXTRA: []].toString()))
    playGame(6)
    assertThat(hanoiTower.rods.toString(), equalTo([FROM: [], TO: [6,5,4,3,2,1], EXTRA: []].toString()))
  }

  @Test
  void playingGameWith10Discs_ShouldReturnCorrectResult() {
    Integer n = 10
    playGame(n)
    def aHundredNumbers = []
    (n..1).each {aHundredNumbers += it}
     assertThat(hanoiTower.rods.toString(), equalTo([FROM: [], TO: aHundredNumbers, EXTRA: []].toString()))
  }
}
